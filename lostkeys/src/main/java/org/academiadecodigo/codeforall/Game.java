package org.academiadecodigo.codeforall;

import org.academiadecodigo.codeforall.gameobjects.*;
import org.academiadecodigo.codeforall.sound.Playsounds;
import org.academiadecodigo.codeforall.sound.Sound;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;


public class Game implements KeyboardHandler {

    private Keyboard keyboard;
    private Grid grid;
    private int delay;
    private GameObject[] gameObjects;
    private Diamond[] diamonds;
    private Rat rat;
    private Chest[] chests;
    private Daddy daddy;
    private Player player;
    private Door door;
    private Chest keychest;
    public static int points = 0;
    private int level;
    private boolean alive = true;
    public static boolean start = false;





    public Game(int delay) {

        grid = new Grid();
        this.player = GameObjectFactory.createPlayer(grid);
        this.delay = delay;



    }


    public void init(){
        grid.init();

        level++;
        if (level ==1){
            Playsounds.playBackground();
        }


        chests = new Chest[5];
        diamonds = new Diamond[10];
        gameObjects = new GameObject[3];

        door = GameObjectFactory.createDoor(grid);
        keychest = GameObjectFactory.createChestWithKey(grid);
        daddy = GameObjectFactory.createDaddy(grid);
        rat = GameObjectFactory.createRat(grid);




        gameObjects[0] = player;
        gameObjects[1] = door;
        gameObjects[2] = keychest;

        for (int i = 0; i < diamonds.length; i++) {
            diamonds[i] = GameObjectFactory.createDiamond(grid);
        }

        for (int i = 0; i < chests.length; i++) {
            chests[i] = GameObjectFactory.createChest(grid);
        }

        check();

    }


    public void newLevel() {
        player.setKey(false);
        init();
    }

    public void check() {
        if (player.getPos().equals(door.getPos())) {
            if (player.hasKey()) {
                door.goThrough();
                this.newLevel();
            }
        }

        if (player.getPos().equals(daddy.getPos())){
            daddy.interact(player);
            if (player.getLives() == 0) {
                player.getPos().setPos(40,40);
                alive = false;
            }
        }

        if (player.getPos().equals(keychest.getPos())) {
            keychest.interact(player);
        }

        for (int i = 0; i < chests.length; i++) {
            if (player.getPos().equals(chests[i].getPos())) {
                chests[i].interact(player);
            }
        }

        for (int i = 0; i < diamonds.length; i++) {
            if (player.getPos().equals(diamonds[i].getPos())) {
                diamonds[i].interact(player);
            }
        }

    }

    public void start() throws InterruptedException {

        Picture startMenu = new Picture(Grid.PADDING,Grid.PADDING,"/image/startmenu.png");

        while (!start){
            startMenu.draw();
        }
        startMenu.delete();

        while (alive && start) {
            Thread.sleep(125);
            check();
            rat.move();
            daddy.move(player);
            updateCounter();
            Thread.sleep(125);
            check();
        }

        Picture endScreen = new Picture(Grid.PADDING,Grid.PADDING,"/image/endmenu.png");
        Playsounds.stopBackground();
        endScreen.draw();
    }

    public Grid getGrid() {
        return grid;
    }

    private void updateCounter(){

        String hearts = "♥ ";
        String heartDisplay = "";

        for (int i = 0; i < player.getLives(); i++) {
            heartDisplay += hearts;
        }

        grid.statusText.delete();
        grid.statusText.setText("Level: "+level+"    ||    Diamonds:       "+points+"         Hearts: "+heartDisplay);
        grid.statusText.draw();
    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_SPACE:
                try {
                    start();
                } catch (InterruptedException ex) {
                    break;
                }
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }
}
