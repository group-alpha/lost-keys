package org.academiadecodigo.codeforall.sound;

public class Playsounds {

    private static Sound DIAMONDS;
    private static Sound BACKGROUND;
    private static Sound CHEST;
    private static Sound DEATH;
    private static Sound DOOR;


    public static void playDiamonds(){
        DIAMONDS = new Sound("/sound/diamond.wav");
        DIAMONDS.play(true);
    }

    public static  void playBackground(){
        BACKGROUND = new Sound("/sound/background_music.wav");
        BACKGROUND.play(true);
        BACKGROUND.setLoop(2);
    }

    public static void stopBackground() {
        BACKGROUND.stop();
    }

    public static void  playChest(){
        CHEST = new Sound("/sound/nokeychest.wav");
        CHEST.play(true);
    }
    public static void  playDeath(){
        DEATH = new Sound("/sound/deathsoundscreaming.wav");
        DEATH.play(true);
    }

    public static void playDoor(){
        DOOR = new Sound("/sound/door.wav");
        DOOR.play(true);
    }
}
