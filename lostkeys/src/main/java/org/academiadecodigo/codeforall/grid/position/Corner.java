package org.academiadecodigo.codeforall.grid.position;

import org.academiadecodigo.codeforall.Grid;

public enum Corner {
    UPPER_LEFT_CORNER(0, 2),
    UPPER_RIGHT_CORNER(24, 2),
    LOWER_LEFT_CORNER(0, 14),
    LOWER_RIGHT_CORNER(24, 14);

    private int col;
    private int row;

    Corner(int col, int row) {
        this.col = col;
        this.row = row;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public static Corner randomCorner() {
        int randomIndex = (int) (Math.floor(Math.random() * 4));

        return values()[randomIndex];

    }


}
