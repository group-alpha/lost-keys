package org.academiadecodigo.codeforall.grid.position;

import org.academiadecodigo.codeforall.Grid;
import org.academiadecodigo.simplegraphics.pictures.Picture;


public class Position {

    private int col;
    private int row;

    private int x;
    private int y;

    private Grid grid;
    private Picture sprite;

    public Position(Grid grid) throws java.util.ConcurrentModificationException {
        try {
            this.grid = grid;

            col = (int) (Math.random() * grid.getCol());
            row = (int) (Math.random() * grid.getRows());

            if (row == 0 || row == 1) {
                this.row = 2;
            }

            if (row >= 14) {
                this.row = 13;
            }


            x = grid.colToX(col);
            y = grid.rowToY(row);

            if (sprite == null) {
                sprite = new Picture(x, y, "/image/nothing.png");
                show();
            }
        } catch (java.util.ConcurrentModificationException e) {
            // I'm sorry Mc's for not doing any better =(
            System.out.println("OMG THE GAME ALMOST BROKE");
        }
    }

    public void setSprite(Picture sprite) {
        this.sprite = sprite;
    }

    public Position(int col, int row, Grid grid) throws java.util.ConcurrentModificationException {
        try {
            this.grid = grid;
            this.col = col;
            this.row = row;

            x = grid.colToX(col);
            y = grid.rowToY(row);


            if (sprite == null) {
                sprite = new Picture(x, y, "/image/nothing.png");
                show();
            }
        } catch (java.util.ConcurrentModificationException e) {
            System.out.println("OMG THE GAME ALMOST BROKE");
        }
    }

    public Grid getGrid() {
        return grid;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public void setPos(int col, int row) {
        this.col = col;
        this.row = row;
        show();
    }

    public void show() throws java.util.ConcurrentModificationException, NullPointerException {
        try {
            sprite.draw();
        } catch (java.util.ConcurrentModificationException e) {
            System.out.println("OMG THE GAME ALMOST BROKE!");
        } catch (NullPointerException e) {
            System.out.println("NULL");
        }
    }

    public void hide() throws java.util.ConcurrentModificationException {
        try {
            sprite.delete();
        } catch (java.util.ConcurrentModificationException e) {
            System.out.println("OMG THE GAME ALMOST BROKE!");
        }
    }

    public void moveInDirection(Direction direction, int speed) throws java.util.ConcurrentModificationException, NullPointerException {
        try {
            int initialCol = getCol();
            int initialRow = getRow();

            switch (direction) {

                case UP:
                    moveUp(speed);
                    break;
                case DOWN:
                    moveDown(speed);
                    break;
                case LEFT:
                    moveLeft(speed);
                    break;
                case RIGHT:
                    moveRight(speed);
                    break;
            }

            int dx = grid.colToX(getCol()) - grid.colToX(initialCol);
            int dy = grid.rowToY(getRow()) - grid.rowToY(initialRow);


            sprite.translate(dx, dy);
        } catch (java.util.ConcurrentModificationException e) {
            System.out.println("OMG THE GAME ALMOST BROKE!");
        } catch (NullPointerException e) {
            System.out.println("NULL");
        }
    }

    public boolean equals(Position pos) {
        return col == pos.getCol() && row == pos.getRow();
    }

    private void moveUp(int speed) {


        int maxRowsUp = Math.min(speed, getRow() - 2);
        setPos(getCol(), getRow() - maxRowsUp);

    }

    private void moveDown(int speed) {

        int maxRowsDown = Math.min(getGrid().getRows() - 2 - (getRow() + 1), speed);
        setPos(getCol(), getRow() + maxRowsDown);

    }

    private void moveLeft(int speed) {

        int maxRowsLeft = Math.min(speed, getCol());
        setPos(getCol() - maxRowsLeft, getRow());

    }

    private void moveRight(int speed) {

        int maxRowsRight = Math.min(getGrid().getCol() - (getCol() + 1), speed);
        setPos(getCol() + maxRowsRight, getRow());
    }

    public void setSprite(String pic) throws java.util.ConcurrentModificationException, NullPointerException {
        try {
            sprite.load(pic);
            hide();
            show();
        } catch (java.util.ConcurrentModificationException e) {
            System.out.println("OMG THE GAME ALMOST BROKE!");
        } catch (NullPointerException e) {
            System.out.println("NULL");
        }
    }
}
