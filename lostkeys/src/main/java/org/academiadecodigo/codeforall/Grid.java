package org.academiadecodigo.codeforall;

import org.academiadecodigo.codeforall.grid.position.Corner;
import org.academiadecodigo.codeforall.grid.position.Position;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Grid {
    public static final int PADDING = 10;


    private int cellSize = 32;
    private int col;
    private int rows;;

    private Picture field;
    private Picture statusBar;

    public Text statusText;

    public Grid() {
        col = 25;
        rows = 17;
    }

    public void init() {
        field = new Picture(PADDING,PADDING, "/image/gamebackdrop.png");
        field.draw();

        statusBar = new Picture(PADDING,PADDING+480,"/image/statusbar.png");
        statusBar.draw();

        statusText = new Text(statusBar.getX()+20,statusBar.getY()+10,"Diamonds ="+Game.points);
        statusText.setColor(Color.WHITE);
        statusText.draw();
    }


    public int getCol() {
        return col;
    }

    public int getRows() {
        return rows;
    }

    public int rowToY(int row){
        return PADDING + cellSize * row;
    }

    public int colToX(int col){
        return PADDING + cellSize * col;
    }

    public Position makeGridPosition() {
        Position position = new Position(this);

        for (int i = 0; i < Corner.values().length; i++) {
            if (Corner.values()[i].getCol() == position.getCol() && Corner.values()[i].getRow() == position.getRow()){
                position = new Position(this);
            }
        }
        return position;
    }

    public Position makeGridPosition(int col, int row) {

        return new Position(col,row, this);
    }

    public Position makeDoorPosition(){

        Corner randomCorner = Corner.randomCorner();

        int col = randomCorner.getCol();
        int row = randomCorner.getRow();

        return new Position(col,row,this);
    }

}
