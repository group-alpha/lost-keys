package org.academiadecodigo.codeforall.gameobjects;

import org.academiadecodigo.codeforall.Grid;
import org.academiadecodigo.codeforall.grid.position.Position;

public abstract class GameObject {
    protected Position pos;
    protected Grid grid;;

    public GameObject(Position pos){
        this.pos = pos;
    }

    public void setGrid(Grid grid) {
        this.grid = grid;
    }

    public Position getPos() {
        return pos;
    }

    public Grid getGrid() {
        return grid;
    }

    public abstract void interact(Player player);
}
