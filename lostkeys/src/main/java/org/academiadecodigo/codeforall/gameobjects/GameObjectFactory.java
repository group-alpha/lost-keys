package org.academiadecodigo.codeforall.gameobjects;

import org.academiadecodigo.codeforall.Grid;
import org.academiadecodigo.codeforall.grid.position.Position;

public class GameObjectFactory  {

    public static Door createDoor(Grid grid) {
        return new Door(grid.makeDoorPosition());
    }

    public static Chest createChest(Grid grid) {
        Position position = grid.makeGridPosition();
        return new Chest(position);
    }

    public static Chest createChestWithKey(Grid grid) {
        Position position = grid.makeGridPosition();
        return new Chest(position, true);
    }

    public static Player createPlayer(Grid grid){
        return new Player(grid.makeGridPosition(1, 2));
    }

    public static Diamond createDiamond(Grid grid) {
        return new Diamond(grid.makeGridPosition());
    }

    public static Rat createRat(Grid grid){
        return new Rat(grid.makeGridPosition());
    }

    public static Daddy createDaddy(Grid grid){
        return new Daddy(grid.makeGridPosition());
    }
}
