package org.academiadecodigo.codeforall.gameobjects;

import org.academiadecodigo.codeforall.Grid;
import org.academiadecodigo.codeforall.grid.position.Position;
import org.academiadecodigo.codeforall.sound.Playsounds;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;

public class Door extends GameObject {

    public Door(Position pos){
        super (pos);
        pos.setSprite("/image/door.png");
    }

    public void goThrough(){
        Playsounds.playDoor();
    }

    @Override
    public void interact(Player player) {
        Playsounds.playDoor();
        goThrough();
    }
}
