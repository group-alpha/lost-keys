package org.academiadecodigo.codeforall.gameobjects;

import org.academiadecodigo.codeforall.grid.position.Direction;
import org.academiadecodigo.codeforall.grid.position.Position;
import org.academiadecodigo.codeforall.sound.Playsounds;

public class Daddy extends GameObject {

    private int speed = 1;
    private Direction currentDirection = Direction.UP;

    public Daddy(Position pos) {
        super(pos);

    }

    public void interact(Player player) {
        Playsounds.playDeath();
        player.setLives(player.getLives()-1);
    }


    public Direction chooseDirection(Player player) {

        Direction newDirection = currentDirection;


        if (player.getPos().getRow() < this.getPos().getRow()) {
            pos.setSprite("/image/monsterback.png");
            newDirection = Direction.UP;
            return newDirection;
        }
        if (player.getPos().getCol() < this.getPos().getCol()) {
            pos.setSprite("/image/monsterleft.png");
            newDirection = Direction.LEFT;
            return newDirection;
        }
        if (player.getPos().getRow() > this.getPos().getRow()) {
            pos.setSprite("/image/monsterfront.png");
            newDirection = Direction.DOWN;
            return newDirection;
        }

        if (player.getPos().getCol() > this.getPos().getCol()) {
            pos.setSprite("/image/monsterright.png");
            newDirection = Direction.RIGHT;
            return newDirection;
        }
        return newDirection;
    }

    public void walk(Direction direction, int speed) {

        Direction newDirection = direction;


        this.currentDirection = newDirection;
        for (int i = 0; i < speed; i++) {
            getPos().moveInDirection(newDirection, speed);
        }
    }

    public void move(Player player) {
        walk(chooseDirection(player), speed);
    }
}
