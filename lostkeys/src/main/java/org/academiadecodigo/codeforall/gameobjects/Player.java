package org.academiadecodigo.codeforall.gameobjects;

import org.academiadecodigo.codeforall.Game;
import org.academiadecodigo.codeforall.grid.position.Direction;
import org.academiadecodigo.codeforall.grid.position.Position;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Player extends GameObject implements KeyboardHandler {

    private Keyboard keyboard;
    private int speed;
    private Direction currentDirection;
    private boolean hasKey;
    private int lives;


    public Player(Position pos) {
        super(pos);
        keyboard = new Keyboard(this);
        hasKey = false;
        lives = 3;
        pos.setSprite("/image/playerfront.png");
        init();

    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public void init() {
        KeyboardEvent left = new KeyboardEvent();
        left.setKey(KeyboardEvent.KEY_LEFT);
        left.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent right = new KeyboardEvent();
        right.setKey(KeyboardEvent.KEY_RIGHT);
        right.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent up = new KeyboardEvent();
        up.setKey(KeyboardEvent.KEY_UP);
        up.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent down = new KeyboardEvent();
        down.setKey(KeyboardEvent.KEY_DOWN);
        down.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);

        KeyboardEvent space = new KeyboardEvent();
        space.setKey(KeyboardEvent.KEY_SPACE);
        space.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);


        keyboard.addEventListener(left);
        keyboard.addEventListener(right);
        keyboard.addEventListener(up);
        keyboard.addEventListener(down);
        keyboard.addEventListener(space);

    }

    public void move(Direction direction, int speed) {
        this.currentDirection = direction;
        for (int i = 0; i < speed; i++) {
            getPos().moveInDirection(direction, 1);
        }
    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {


        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_LEFT:
                Game.start=true;
                currentDirection = Direction.LEFT;
                if (hasKey) {
                    pos.setSprite("/image/playerleftkey.png");
                    break;
                }
                pos.setSprite("/image/playerleft.png");
                break;
            case KeyboardEvent.KEY_RIGHT:
                Game.start=true;
                currentDirection = Direction.RIGHT;
                if (hasKey) {
                    pos.setSprite("/image/playerrightkey.png");
                    break;
                }
                pos.setSprite("/image/playerright.png");
                break;
            case KeyboardEvent.KEY_UP:
                Game.start=true;
                currentDirection = Direction.UP;
                if (hasKey) {
                    pos.setSprite("/image/playerbackkey.png");
                    break;
                }
                pos.setSprite("/image/playerback.png");
                break;
            case KeyboardEvent.KEY_DOWN:
                Game.start=true;
                currentDirection = Direction.DOWN;
                if (hasKey) {
                    pos.setSprite("/image/playerfrontkey.png");
                    break;
                }
                pos.setSprite("/image/playerfront.png");
                break;
            case KeyboardEvent.KEY_SPACE:
                Game.start=true;
                break;
        }

        if (speed == 0) {
            move(currentDirection, 1);
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }

    public void setKey(boolean hasKey) {
        this.hasKey = hasKey;
    }

    public boolean hasKey() {
        return hasKey;
    }

    @Override
    public void interact(Player player) {
    }

}
