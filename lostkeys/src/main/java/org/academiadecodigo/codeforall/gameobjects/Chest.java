package org.academiadecodigo.codeforall.gameobjects;

import org.academiadecodigo.codeforall.grid.position.Position;
import org.academiadecodigo.codeforall.sound.Playsounds;

public class Chest extends GameObject {

    private boolean hasKey;
    private boolean isKeyChest;
    private boolean opened;


    public Chest(Position pos){
        super (pos);
        hasKey = true;
        pos.setSprite("/image/chestclosed.png");
    }

    public Chest (Position pos, boolean hasKey){
        super (pos);
        this.hasKey = hasKey;
        isKeyChest = true;
        pos.setSprite("/image/chestclosed.png");
    }

    public void open(Player player) {
        if (!opened) {
            if (isKeyChest) {
                player.setKey(true);
                hasKey = false;
                pos.setSprite("/image/redchestopen.png");
                System.out.println("Player has key set to " + player.hasKey() + " Chest has key set to " + hasKey);
                opened = true;
                Playsounds.playChest();
                return;
            }
            opened = true;
            pos.setSprite("/image/chestopen.png");
            Playsounds.playChest();
        }
    }

    @Override
    public void interact(Player player) {
        open(player);
    }
}
