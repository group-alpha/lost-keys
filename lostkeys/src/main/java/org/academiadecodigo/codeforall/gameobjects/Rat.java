package org.academiadecodigo.codeforall.gameobjects;

import org.academiadecodigo.codeforall.grid.position.Direction;
import org.academiadecodigo.codeforall.grid.position.Position;

public class Rat extends GameObject{

    private int speed = 1;
    private Direction currentDirection = Direction.UP;
    private int directionChangeLevel = 5;

    public Rat(Position pos) {
        super(pos);

    }

    public void interact(Player player){};


    public Direction chooseDirection() {

        Direction newDirection = currentDirection;

        if (Math.random() > ((double) directionChangeLevel) / 10) {
            newDirection = Direction.values()[(int) (Math.random() * Direction.values().length)];
        }


        switch (newDirection) {
            case UP:
                pos.setSprite("/image/ratback.png");
                break;
            case DOWN:
                pos.setSprite("/image/ratfront.png");
                break;
            case LEFT:
                pos.setSprite("/image/ratleft.png");
                break;
            case RIGHT:
                pos.setSprite("/image/ratright.png");
                break;
        }
        return newDirection;

    }

    public void walk(Direction direction, int speed) {

        Direction newDirection = direction;
        this.currentDirection = newDirection;
        getPos().moveInDirection(newDirection, speed);
    }

    public void move() {
        walk(chooseDirection(), speed);
    }
}



