package org.academiadecodigo.codeforall.gameobjects;

import org.academiadecodigo.codeforall.Game;
import org.academiadecodigo.codeforall.grid.position.Position;
import org.academiadecodigo.codeforall.sound.Playsounds;

public class Diamond extends GameObject {

    private boolean caught;

    public Diamond(Position pos){
        super (pos);
        caught = false;
        pos.setSprite("/image/diamond.png");
    }

     @Override
    public void interact(Player player) {
        if (!caught){
            getPos().setSprite("/image/nothing.png");
            Game.points++;
            caught = true;
            if (Game.points % 20 == 0){
                player.setLives(player.getLives()+1);
            }
            System.out.println(Game.points);
            Playsounds.playDiamonds();
        }

    }
}
