package org.academiadecodigo.codeforall;

public class Main {

    public static void main(String[] args) {

        Game game = new Game(250);
        game.init();

        try {
            game.start();
        } catch (InterruptedException ex) {
        }
    }
}
